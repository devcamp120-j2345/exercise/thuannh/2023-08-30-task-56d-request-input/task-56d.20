package com.devcamp.countryregionapi.service;

import java.util.ArrayList;

import org.springframework.stereotype.Service;

import com.devcamp.countryregionapi.model.Region;

@Service
public class RegionService {
    Region hanoi = new Region("HN", "Ha Noi");
    Region hochiminh = new Region("HCM", "Ho Chi Minh");
    Region danang = new Region("DN", "Da Nang");

    Region newyork = new Region("NY", "New York");
    Region california = new Region("CAL", "Califonia");
    Region florida = new Region("FLO", "Florida");

    Region moscow = new Region("MC", "Moscow");
    Region kaluga = new Region("KL", "Kaluga");
    Region saintpeterburg = new Region("SP", "Saint Peterburg");

    public ArrayList<Region> getRegionVN(){
        ArrayList<Region> regionsVN = new ArrayList<>();

        regionsVN.add(hanoi);
        regionsVN.add(hochiminh);
        regionsVN.add(danang);

        return regionsVN;
    }

    public ArrayList<Region> getRegionUS(){
        ArrayList<Region> regionsUS = new ArrayList<>();

        regionsUS.add(newyork);
        regionsUS.add(california);
        regionsUS.add(florida);

        return regionsUS;
    }

    public ArrayList<Region> getRegionRussia(){
        ArrayList<Region> regionsRus = new ArrayList<>();

        regionsRus.add(moscow);
        regionsRus.add(kaluga);
        regionsRus.add(saintpeterburg);

        return regionsRus;
    }



    //cách 2: dồn hết việc cho service
    public Region filterRegions(String regionCode){
        ArrayList<Region> regions = new ArrayList<>();

        regions.add(hanoi);
        regions.add(hochiminh);
        regions.add(danang);
        regions.add(newyork);
        regions.add(california);
        regions.add(florida);
        regions.add(moscow);
        regions.add(kaluga);
        regions.add(saintpeterburg);

        Region findRegion = new Region();

        for (Region regionElement : regions) {
            if(regionElement.getRegionCode().equals(regionCode)){
                findRegion = regionElement;
            }
        }
        return findRegion;
    }
}
