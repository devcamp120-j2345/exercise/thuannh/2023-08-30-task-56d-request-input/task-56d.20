package com.devcamp.countryregionapi.service;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.countryregionapi.model.Country;

@Service
public class CountryService {
    @Autowired
    private RegionService regionService;

    Country vn = new Country("VN", "Viet Nam");
    Country us = new Country("US", "USA");
    Country rus = new Country("RUS", "Russia");


    public ArrayList<Country> getAllCountries(){
        ArrayList<Country> allCountry = new ArrayList<>();

        vn.setRegion(regionService.getRegionVN());
        us.setRegion(regionService.getRegionUS());
        rus.setRegion(regionService.getRegionRussia());


        allCountry.add(vn);
        allCountry.add(us);
        allCountry.add(rus);

        return allCountry;
    }
    public  Country getCountriesByIndex(int index){
        ArrayList<Country> Country = new ArrayList<>();

        vn.setRegion(regionService.getRegionVN());
        us.setRegion(regionService.getRegionUS());
        rus.setRegion(regionService.getRegionRussia());


        Country.add(vn);
        Country.add(us);
        Country.add(rus);
        if(index >= 0 || index < Country.size()){
            return Country.get(index);
        }
        return null;
    }

}
